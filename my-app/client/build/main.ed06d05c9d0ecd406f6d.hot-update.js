exports.id = "main";
exports.modules = {

/***/ "./node_modules/@deity/falcon-client/src/clientApp/defaultConfiguration.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@deity/falcon-client/src/clientApp/defaultConfiguration.js ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var deepmerge__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! deepmerge */ "./node_modules/deepmerge/dist/es.js");

/* harmony default export */ __webpack_exports__["default"] = (initialConfig => Object(deepmerge__WEBPACK_IMPORTED_MODULE_0__["default"])({
  __typename: 'ClientConfig',
  logLevel: 'error',
  serverSideRendering: true,
  graphqlUrl: 'http://pwa-falcon.testingnow.me:4000/graphql',
  apolloClient: {
    __typename: 'ApolloClientConfig',
    httpLink: {
      __typename: 'ApolloClientLinkConfig',
      uri: '/graphql',
      useGETForQueries: false
    },
    connectToDevTools: "development" !== 'production',
    defaultOptions: {
      __typename: 'ApolloClientDefaultOptions'
    },
    queryDeduplication: true
  },
  googleTagManager: {
    __typename: 'GTMConfig',
    id: null
  },
  i18n: {
    __typename: 'I18nConfig',
    lng: 'en',
    ns: ['common'],
    fallbackLng: 'en',
    whitelist: ['en'],
    // available: languages taken from falcon-server
    debug: false
  }
}, initialConfig || {}, {
  arrayMerge: (destination, source) => source,
  clone: false
}));

/***/ }),

/***/ "./node_modules/@deity/falcon-client/src/service/ApolloClient.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@deity/falcon-client/src/service/ApolloClient.js ***!
  \***********************************************************************/
/*! exports provided: ApolloClient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApolloClient", function() { return ApolloClient; });
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ "./node_modules/@babel/runtime/helpers/objectSpread.js");
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/objectWithoutProperties */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var apollo_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-link */ "./node_modules/apollo-link/lib/bundle.esm.js");
/* harmony import */ var apollo_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! apollo-client */ "./node_modules/apollo-client/bundle.esm.js");
/* harmony import */ var apollo_link_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! apollo-link-http */ "./node_modules/apollo-link-http/lib/bundle.esm.js");
/* harmony import */ var apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-cache-inmemory */ "./node_modules/apollo-cache-inmemory/lib/bundle.esm.js");
/* harmony import */ var node_fetch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! node-fetch */ "./node_modules/node-fetch/lib/index.mjs");
/* harmony import */ var deepmerge__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! deepmerge */ "./node_modules/deepmerge/dist/es.js");








/**
 * @typedef {object} FalconApolloClientConfig
 * @property {boolean} [isBrowser=false] Boolean flag to determine the current environment
 * @property {object} [initialState={}] Object to restore Cache data from
 * @property {string} [serverUri="http://pwa-falcon.testingnow.me:4000/graphql"] ApolloServer URL
 * @property {FalconApolloClientStateConfig} [clientState={}] Configuration of client state for Apollo
 */

/**
 * @typedef {object} FalconApolloClientStateConfig
 * @property {object} data https://www.apollographql.com/docs/react/essentials/local-state.html#cache-initialization
 * @property {object} resolvers https://www.apollographql.com/docs/react/essentials/local-state.html#local-resolvers
 */

/**
 * Creates an ApolloClient instance with the provided arguments
 * @param {FalconApolloClientConfig} config Falcon configuration for creating ApolloClient instance
 * @return {Apollo} ApolloClient instance
 */

function ApolloClient(config = {}) {
  const {
    extraLinks = [],
    isBrowser = false,
    initialState = {},
    clientState = {},
    headers,
    apolloClientConfig,
    cache
  } = config;

  const {
    httpLink,
    connectToDevTools
  } = apolloClientConfig,
        restApolloClientConfig = _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default()(apolloClientConfig, ["httpLink", "connectToDevTools"]);

  const inMemoryCache = cache || new apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_5__["InMemoryCache"]().restore(initialState);
  inMemoryCache.writeData({
    data: clientState.data
  });
  let httpLinkUri = httpLink.uri;

  if (!isBrowser && clientState.data.config.graphqlUrl) {
    httpLinkUri = clientState.data.config.graphqlUrl;
  }

  const apolloHttpLink = Object(apollo_link_http__WEBPACK_IMPORTED_MODULE_4__["createHttpLink"])(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default()({}, httpLink, {
    uri: httpLinkUri,
    fetch: node_fetch__WEBPACK_IMPORTED_MODULE_6__["default"],
    credentials: 'include',
    headers
  }));
  const client = new apollo_client__WEBPACK_IMPORTED_MODULE_3__["default"](deepmerge__WEBPACK_IMPORTED_MODULE_7__["default"].all([{
    ssrMode: !isBrowser,
    cache: inMemoryCache,
    link: apollo_link__WEBPACK_IMPORTED_MODULE_2__["ApolloLink"].from([...extraLinks, apolloHttpLink]),
    connectToDevTools: isBrowser && connectToDevTools,
    resolvers: clientState.resolvers
  }, restApolloClientConfig], {
    clone: false
  }));
  client.onResetStore(() => cache.writeData({
    data: clientState.data
  }));
  return client;
}

/***/ })

};
//# sourceMappingURL=main.ed06d05c9d0ecd406f6d.hot-update.js.map