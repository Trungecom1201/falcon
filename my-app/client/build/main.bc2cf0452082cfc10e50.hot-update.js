exports.id = "main";
exports.modules = {

/***/ "./src/pages/shop/components/SnapPayment.js":
/*!**************************************************!*\
  !*** ./src/pages/shop/components/SnapPayment.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _deity_falcon_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @deity/falcon-ui */ "./node_modules/@deity/falcon-ui/dist/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
var _jsxFileName = "/var/www/pwa.test/public_html/my-app/client/src/pages/shop/components/SnapPayment.js";




const SERVER_HTTP = {
  linkUrl: 'http://pwa-falcon.testingnow.me:4000/graphql'
};

class SnapPayment extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderId: this.props.orderid,
      quoteId: this.props.quoteId,
      redirect: ''
    };
  }

  componentWillMount() {
    const snapPayment = window.snap;
    const {
      orderId,
      quoteId
    } = this.state;
    const snapPaymentOb = this;
    snapPayment.pay(this.props.snaptoken, {
      // Optional
      onSuccess(result) {
        if (result.va_numbers != null) {
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          vacodeSnap(va: ${result.va_numbers[0].va_number}, oid : ${result.order_id}) {
            success
          }
        }`).then(res => {
            if (res.data) {
              snapPaymentOb.setState({
                redirect: 'confirmation'
              });
            }
          });
        }

        snapPaymentOb.setState({
          redirect: 'confirmation'
        });
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          removeCart {
            success
          } 
        }`).then(res => {
          if (res.data) {
            console.log(res.data);
          }
        });
      },

      // Optional
      onPending(result) {
        if (result.va_numbers != null) {
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          vacodeSnap(va: ${result.va_numbers[0].va_number}, oid : ${result.order_id}) {
            success
          }
        }`).then(res => {
            if (res.data) {
              snapPaymentOb.setState({
                redirect: 'confirmation'
              });
            }
          });
        }

        snapPaymentOb.setState({
          redirect: 'confirmation'
        });
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          removeCart {
            success
          } 
        }`).then(res => {
          if (res.data) {
            console.log(res.data);
          }
        });
      },

      // Optional
      onError(result) {
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          failSnap(order_id: ${orderId}, quote_id: "${quoteId}") {
            redirect
            quote_id
          }
        }`).then(response => {
          console.log(result.message);
          console.log(response.data);
          snapPaymentOb.setState({
            redirect: 'cart'
          });
        });
      },

      onClose() {
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          failSnap(order_id: ${orderId}, quote_id: "${quoteId}") {
            redirect
            quote_id
          }
        }`).then(response => {
          console.log(response.data);
          snapPaymentOb.setState({
            redirect: 'cart'
          });
        });
      }

    });
  }

  render() {
    if (this.state.redirect === 'confirmation') {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Redirect"], {
        to: "/checkout/confirmation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        }
      });
    }

    if (this.state.redirect === 'cart') {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Redirect"], {
        to: {
          pathname: '/cart',
          state: {
            id: 'snap',
            order_id: this.state.orderId
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        }
      });
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_2__["Box"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      }
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (SnapPayment);

/***/ })

};
//# sourceMappingURL=main.bc2cf0452082cfc10e50.hot-update.js.map