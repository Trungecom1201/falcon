exports.id = "main";
exports.modules = {

/***/ "./src/pages/shop/Checkout/Checkout.js":
/*!*********************************************!*\
  !*** ./src/pages/shop/Checkout/Checkout.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @deity/falcon-ui */ "./node_modules/@deity/falcon-ui/dist/index.js");
/* harmony import */ var _deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @deity/falcon-ecommerce-uikit */ "./node_modules/@deity/falcon-ecommerce-uikit/dist/index.js");
/* harmony import */ var _CheckoutCartSummary__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./CheckoutCartSummary */ "./src/pages/shop/Checkout/CheckoutCartSummary.js");
/* harmony import */ var _CustomerSelector__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./CustomerSelector */ "./src/pages/shop/Checkout/CustomerSelector.js");
/* harmony import */ var _ShippingMethodSection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ShippingMethodSection */ "./src/pages/shop/Checkout/ShippingMethodSection.js");
/* harmony import */ var _PaymentMethodSection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./PaymentMethodSection */ "./src/pages/shop/Checkout/PaymentMethodSection.js");
/* harmony import */ var _AddressSection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./AddressSection */ "./src/pages/shop/Checkout/AddressSection.js");
/* harmony import */ var _components_SnapPayment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/SnapPayment */ "./src/pages/shop/components/SnapPayment.js");
/* harmony import */ var _components_ErrorList__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/ErrorList */ "./src/pages/shop/components/ErrorList.js");
/* harmony import */ var _components_SnapQuery__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/SnapQuery */ "./src/pages/shop/components/SnapQuery.js");

var _jsxFileName = "/var/www/pwa.test/public_html/my-app/client/src/pages/shop/Checkout/Checkout.js";













const CHECKOUT_STEPS = {
  EMAIL: 'EMAIL',
  SHIPPING_ADDRESS: 'SHIPPING_ADDRESS',
  BILLING_ADDRESS: 'BILLING_ADDRESS',
  SHIPPING: 'SHIPPING',
  PAYMENT: 'PAYMENT',
  CONFIRMATION: 'CONFIRMATION'
};
const CheckoutArea = {
  checkout: 'checkout',
  cart: 'cart',
  divider: 'divider'
};
const checkoutLayout = {
  checkoutLayout: {
    display: 'grid',
    gridGap: 'lg',
    my: 'lg',
    px: {
      sm: 'none',
      md: 'xxxl'
    },
    // prettier-ignore
    gridTemplate: {
      xs: Object(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["toGridTemplate"])([['1fr'], [CheckoutArea.checkout], [CheckoutArea.divider], [CheckoutArea.cart]]),
      md: Object(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["toGridTemplate"])([['2fr', '1px', '1fr'], [CheckoutArea.checkout, CheckoutArea.divider, CheckoutArea.cart]])
    },
    css: ({
      theme
    }) => ({
      // remove default -/+ icons in summary element
      'details summary:after': {
        display: 'none'
      },
      'details summary:active, details summary:focus': {
        outline: 'none'
      },
      'details summary': {
        paddingRight: theme.spacing.xxl
      },
      'details article': {
        paddingLeft: theme.spacing.xxl,
        paddingRight: theme.spacing.xxl
      }
    })
  }
}; // loader that covers content of the container (container must have position: relative/absolute)

const Loader = ({
  visible
}) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
  css: {
    position: 'absolute',
    display: visible ? 'flex' : 'none',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'rgba(255, 255, 255, 0.7)',
    height: '100%',
    width: '100%'
  },
  __source: {
    fileName: _jsxFileName,
    lineNumber: 80
  }
}, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Icon"], {
  src: "loader",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 91
  }
})); // helper that computes step that should be open based on values from CheckoutLogic


const computeStepFromValues = (values, errors) => {
  if (!values.email || errors.email) {
    return CHECKOUT_STEPS.EMAIL;
  } else if (!values.shippingAddress || errors.shippingAddress) {
    return CHECKOUT_STEPS.SHIPPING_ADDRESS;
  } else if (!values.billingAddress || errors.billingAddress) {
    return CHECKOUT_STEPS.BILLING_ADDRESS;
  } else if (!values.shippingMethod || errors.shippingMethod) {
    return CHECKOUT_STEPS.SHIPPING;
  } else if (!values.paymentMethod || errors.paymentMethod) {
    return CHECKOUT_STEPS.PAYMENT;
  }

  return CHECKOUT_STEPS.CONFIRMATION;
};

const SERVER_HTTP = {
  linkUrl: 'http://pwa-falcon.testingnow.me:4000/graphql'
};

class CheckoutWizard extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(this, "setCurrentStep", currentStep => this.setState({
      currentStep
    }));

    this.state = {
      currentStep: CHECKOUT_STEPS.EMAIL,
      getCurrentProps: () => this.props // eslint-disable-line react/no-unused-state

    };
  }

  componentDidMount() {
    const script = document.createElement('script');
    axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(`${SERVER_HTTP.linkUrl}?query={
          infoSnap {
            is_production
            client_id
            is_active
          }
        }`).then(res => {
      const clientId = res.data.data.infoSnap.client_id;
      const isProduction = res.data.data.infoSnap.is_production;
      const isActive = res.data.data.infoSnap.is_active;

      if (isProduction !== 0) {
        script.src = 'https://app.midtrans.com/snap/snap.js';
      } else {
        script.src = 'https://app.sandbox.midtrans.com/snap/snap.js';
      }

      script.async = true;
      script.setAttribute('data-client-key', clientId);

      if (isActive === 1) {
        document.body.appendChild(script);
      }
    });
  }

  static getDerivedStateFromProps(nextProps, currentState) {
    const {
      checkoutData: currentPropsData
    } = currentState.getCurrentProps();
    const currentStepFromProps = computeStepFromValues(currentPropsData.values, currentPropsData.errors);
    const nextStepFromProps = computeStepFromValues(nextProps.checkoutData.values, nextProps.checkoutData.errors);
    const changedStep = {
      currentStep: nextStepFromProps
    }; // if there's no step set yet then set it correctly

    if (!currentState.currentStep) {
      return changedStep;
    } // if loading has finished (changed from true to false) and there's no error then enforce current step
    // to value computed from the next props - this ensures that if user requested edit of particular step
    // then and it has been processed then we want to display step based on actual values from CheckoutLogic


    if (currentPropsData.loading && !nextProps.checkoutData.loading && !nextProps.checkoutData.error) {
      return changedStep;
    } // if step computed from props has changed then use it as new step


    if (nextStepFromProps !== currentStepFromProps) {
      return changedStep;
    }

    return null;
  }

  render() {
    const {
      currentStep
    } = this.state;
    const {
      values,
      loading,
      errors,
      orderId,
      availableShippingMethods,
      availablePaymentMethods,
      setEmail,
      setShippingAddress,
      setBillingAddress,
      setBillingSameAsShipping,
      setShippingMethod,
      setPaymentMethod,
      placeOrder
    } = this.props.checkoutData;
    const {
      customerData
    } = this.props;
    let addresses;
    let defaultShippingAddress;
    let defaultBillingAddress; // detect if user is logged in - if so and he has address list then use it for address sections

    if (customerData && customerData.addresses && customerData.addresses.length) {
      ({
        addresses
      } = customerData);
      defaultShippingAddress = addresses.find(item => item.defaultShipping);
      defaultBillingAddress = addresses.find(item => item.defaultBilling);
    }

    if (orderId) {
      if (values.paymentMethod.code === 'snap') {
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CountriesQuery"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 217
          }
        }, ({
          countries
        }) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CartQuery"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 219
          }
        }, ({
          cart
        }) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
          defaultTheme: checkoutLayout,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 221
          }
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
          gridArea: CheckoutArea.cart,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 222
          }
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["H2"], {
          fontSize: "md",
          mb: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 223
          }
        }, "Summary"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_CheckoutCartSummary__WEBPACK_IMPORTED_MODULE_6__["default"], {
          cart: cart,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 226
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Button"], {
          as: react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"],
          mt: "md",
          to: "/cart",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 227
          }
        }, "Edit cart items")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          gridArea: CheckoutArea.divider,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 231
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
          gridArea: CheckoutArea.checkout,
          position: "relative",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 232
          }
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Loader, {
          visible: loading,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 233
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_CustomerSelector__WEBPACK_IMPORTED_MODULE_7__["default"], {
          open: currentStep === CHECKOUT_STEPS.EMAIL,
          onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.EMAIL),
          email: values.email,
          setEmail: setEmail,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 234
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          my: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 241
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddressSection__WEBPACK_IMPORTED_MODULE_10__["default"], {
          id: "shipping-address",
          open: currentStep === CHECKOUT_STEPS.SHIPPING_ADDRESS,
          countries: countries.items,
          onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.SHIPPING_ADDRESS),
          title: "Shipping address",
          submitLabel: "Continue",
          selectedAddress: values.shippingAddress,
          setAddress: setShippingAddress,
          errors: errors.shippingAddress,
          availableAddresses: addresses,
          defaultSelected: defaultShippingAddress,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 243
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          my: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 257
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddressSection__WEBPACK_IMPORTED_MODULE_10__["default"], {
          id: "billing-address",
          open: currentStep === CHECKOUT_STEPS.BILLING_ADDRESS,
          onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.BILLING_ADDRESS),
          title: "Billing address",
          submitLabel: "Continue",
          countries: countries.items,
          selectedAddress: values.billingAddress,
          setAddress: setBillingAddress,
          setUseTheSame: setBillingSameAsShipping,
          useTheSame: values.billingSameAsShipping,
          labelUseTheSame: "My billing address is the same as shipping",
          availableAddresses: addresses,
          defaultSelected: defaultBillingAddress,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 259
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          my: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 275
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ShippingMethodSection__WEBPACK_IMPORTED_MODULE_8__["default"], {
          open: currentStep === CHECKOUT_STEPS.SHIPPING,
          onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.SHIPPING),
          shippingAddress: values.shippingAddress,
          selectedShipping: values.shippingMethod,
          setShippingAddress: setShippingAddress,
          availableShippingMethods: availableShippingMethods,
          setShipping: setShippingMethod,
          errors: errors.shippingMethod,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 277
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          my: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 288
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_PaymentMethodSection__WEBPACK_IMPORTED_MODULE_9__["default"], {
          open: currentStep === CHECKOUT_STEPS.PAYMENT,
          onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.PAYMENT),
          selectedPayment: values.paymentMethod,
          availablePaymentMethods: availablePaymentMethods,
          setPayment: setPaymentMethod,
          errors: errors.paymentMethod,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 290
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
          my: "md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 299
          }
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_SnapQuery__WEBPACK_IMPORTED_MODULE_13__["SnapQuery"], {
          variables: {
            orderId
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 300
          }
        }, ({
          snapToken
        }) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 306
          }
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_SnapPayment__WEBPACK_IMPORTED_MODULE_11__["default"], {
          quoteId: snapToken.quoteId,
          snaptoken: snapToken.name,
          orderid: orderId,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 307
          }
        }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_ErrorList__WEBPACK_IMPORTED_MODULE_12__["default"], {
          errors: errors.order,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 311
          }
        }), currentStep === CHECKOUT_STEPS.CONFIRMATION && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Button"], {
          onClick: placeOrder,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 312
          }
        }, "Place order")))));
      } // order has been placed successfully so we show confirmation


      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Redirect"], {
        to: "/checkout/confirmation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 322
        }
      });
    }

    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CountriesQuery"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 326
      }
    }, ({
      countries
    }) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CartQuery"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 328
      }
    }, ({
      cart
    }) => {
      // cart is empty so redirect user to the homepage
      if (cart.itemsQty === 0) {
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Redirect"], {
          to: "/",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 332
          }
        });
      }

      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
        defaultTheme: checkoutLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 336
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
        gridArea: CheckoutArea.cart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 337
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["H2"], {
        fontSize: "md",
        mb: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 338
        }
      }, "Summary"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_CheckoutCartSummary__WEBPACK_IMPORTED_MODULE_6__["default"], {
        cart: cart,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 341
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        as: react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"],
        mt: "md",
        to: "/cart",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 342
        }
      }, "Edit cart items")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        gridArea: CheckoutArea.divider,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 346
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Box"], {
        gridArea: CheckoutArea.checkout,
        position: "relative",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Loader, {
        visible: loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_CustomerSelector__WEBPACK_IMPORTED_MODULE_7__["default"], {
        open: currentStep === CHECKOUT_STEPS.EMAIL,
        onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.EMAIL),
        email: values.email,
        setEmail: setEmail,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 349
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        my: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 356
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddressSection__WEBPACK_IMPORTED_MODULE_10__["default"], {
        id: "shipping-address",
        open: currentStep === CHECKOUT_STEPS.SHIPPING_ADDRESS,
        countries: countries.items,
        onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.SHIPPING_ADDRESS),
        title: "Shipping address",
        submitLabel: "Continue",
        selectedAddress: values.shippingAddress,
        setAddress: setShippingAddress,
        errors: errors.shippingAddress,
        availableAddresses: addresses,
        defaultSelected: defaultShippingAddress,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 358
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        my: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 372
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddressSection__WEBPACK_IMPORTED_MODULE_10__["default"], {
        id: "billing-address",
        open: currentStep === CHECKOUT_STEPS.BILLING_ADDRESS,
        onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.BILLING_ADDRESS),
        title: "Billing address",
        submitLabel: "Continue",
        countries: countries.items,
        selectedAddress: values.billingAddress,
        setAddress: setBillingAddress,
        setUseTheSame: setBillingSameAsShipping,
        useTheSame: values.billingSameAsShipping,
        labelUseTheSame: "My billing address is the same as shipping",
        availableAddresses: addresses,
        defaultSelected: defaultBillingAddress,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 374
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        my: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 390
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ShippingMethodSection__WEBPACK_IMPORTED_MODULE_8__["default"], {
        open: currentStep === CHECKOUT_STEPS.SHIPPING,
        onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.SHIPPING),
        shippingAddress: values.shippingAddress,
        selectedShipping: values.shippingMethod,
        setShippingAddress: setShippingAddress,
        availableShippingMethods: availableShippingMethods,
        setShipping: setShippingMethod,
        errors: errors.shippingMethod,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 392
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        my: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 403
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_PaymentMethodSection__WEBPACK_IMPORTED_MODULE_9__["default"], {
        open: currentStep === CHECKOUT_STEPS.PAYMENT,
        onEditRequested: () => this.setCurrentStep(CHECKOUT_STEPS.PAYMENT),
        selectedPayment: values.paymentMethod,
        availablePaymentMethods: availablePaymentMethods,
        setPayment: setPaymentMethod,
        errors: errors.paymentMethod,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 405
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
        my: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 414
        }
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_ErrorList__WEBPACK_IMPORTED_MODULE_12__["default"], {
        errors: errors.order,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 416
        }
      }), currentStep === CHECKOUT_STEPS.CONFIRMATION && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ui__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        onClick: placeOrder,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 417
        }
      }, "Place order")));
    }));
  }

}

const CheckoutPage = () => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CheckoutLogic"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 430
  }
}, checkoutData => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["CustomerQuery"], {
  query: _deity_falcon_ecommerce_uikit__WEBPACK_IMPORTED_MODULE_5__["GET_CUSTOMER_WITH_ADDRESSES"],
  __source: {
    fileName: _jsxFileName,
    lineNumber: 432
  }
}, ({
  customer
}) => react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CheckoutWizard, {
  checkoutData: checkoutData,
  customerData: customer,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 433
  }
})));

/* harmony default export */ __webpack_exports__["default"] = (CheckoutPage);

/***/ })

};
//# sourceMappingURL=main.e0b9697f4e3f3e85f482.hot-update.js.map