/* global workbox, self, importScripts */

importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

workbox.core.setCacheNameDetails({ prefix: '@deity' });

workbox.skipWaiting();
workbox.clientsClaim();

workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute([
  {
    "url": "/favicon.ico",
    "revision": "cbb0234c5769740346972362ccfc0e93"
  },
  {
    "url": "/static/js/account.6406d8e7.chunk.js"
  },
  {
    "url": "/static/js/blog/blog.85f8fc2a.chunk.js"
  },
  {
    "url": "/static/js/blog/post.a59a6c87.chunk.js"
  },
  {
    "url": "/static/js/client.e8dd3208.js"
  },
  {
    "url": "/static/js/i18n/en-translations.9fafc51c.chunk.js"
  },
  {
    "url": "/static/js/i18n/nl-translations.bdff06ea.chunk.js"
  },
  {
    "url": "/static/js/polyfills.2ca357d6.chunk.js"
  },
  {
    "url": "/static/js/shop/cart.fe7b3f60.chunk.js"
  },
  {
    "url": "/static/js/shop/category.c60a10d5.chunk.js"
  },
  {
    "url": "/static/js/shop/checkout.c0f2b318.chunk.js"
  },
  {
    "url": "/static/js/vendors.a37fde2d.chunk.js"
  },
  {
    "url": "/static/js/vendors~account.35314b45.chunk.js"
  },
  {
    "url": "/static/js/vendors~account~shop/cart~shop/checkout.90a37dac.chunk.js"
  },
  {
    "url": "/static/js/vendors~shop/cart.240e6cc7.chunk.js"
  },
  {
    "url": "/static/js/vendors~shop/checkout.966f444f.chunk.js"
  },
  {
    "url": "/static/media/128x128.fe1e4d0b.png"
  },
  {
    "url": "/static/media/144x144.8ef8602f.png"
  },
  {
    "url": "/static/media/152x152.c69c1f35.png"
  },
  {
    "url": "/static/media/192x192.e9b2ed19.png"
  },
  {
    "url": "/static/media/384x384.4e3d6609.png"
  },
  {
    "url": "/static/media/512x512.19ff6dd4.png"
  },
  {
    "url": "/static/media/72x72.5ef3566f.png"
  },
  {
    "url": "/static/media/96x96.52899069.png"
  },
  {
    "url": "/static/media/logo.f95b6250.png"
  },
  {
    "url": "/app-shell",
    "revision": "cf45f89eba7d1133ee04ebf32e63dbbf"
  }
], {});

workbox.routing.registerRoute(
  ({ event }) => event.request.mode === 'navigate',
  ({ url }) => fetch(url.href).catch(() => caches.match('/app-shell'))
);

workbox.routing.registerRoute('/', workbox.strategies.networkFirst(), 'GET');
